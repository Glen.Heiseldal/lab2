package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    private List<FridgeItem> itemsInFridge = new ArrayList<FridgeItem>();
    private final int maxSize = 20;

    @Override
    public int nItemsInFridge() {  
        // System.out.println(itemsInFridge.size());
        return itemsInFridge.size();
    }
    
    @Override
    public int totalSize() {  
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge()>=maxSize){
        return false;
        }
        else {
            itemsInFridge.add(item);
            return true;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(nItemsInFridge()>0){
            itemsInFridge.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        itemsInFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> temp = new ArrayList<FridgeItem>();
        for(int i = 0; i<nItemsInFridge(); i++){
            FridgeItem item = itemsInFridge.get(i);
            if(item.hasExpired()){
                temp.add(item);
            }
        }
        itemsInFridge.removeAll(temp);
        return temp;
    }
    
}
